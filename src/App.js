import React from 'react';
import './css/default.less';

import UserInterface from './components/layout/UserInterface';
import ActionInterface from './components/layout/ActionInterface';

function App() {
  return (
    <div className="App">
        <ActionInterface />
        <UserInterface />
    </div>
  );
}

export default App;
