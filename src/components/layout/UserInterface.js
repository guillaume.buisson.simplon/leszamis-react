import React from "react";
import ProfilInterface from "./profilInterface";

export default function UserInterface() {
    return(
        <div className="userInterface">
            <ProfilInterface/>
            <div className="bloc_gestion">
                <div>
                    <div className="bloc_navigation">
                        <ul className="navigation">
                            <li>
                                <a href="index.php" className="i_home">
                                    <span className="visuallyhidden">Accueil</span>
                                </a>
                            </li>
                              {/* POUR LA V2 { */}
                                <li>
                                    <a href="/chat" className="i_user">
                                        <span className="visuallyhidden">Conversation</span>
                                    </a>
                                </li>
                               {/* } */}
                            <li>
                                <a href="/group" className="i_group">
                                    <span className="visuallyhidden">Groupe</span>
                                </a>
                            </li>
                            {/* POUR LA V2 */}
                                <li>
                                    <a href="/calendar" className="i_calendar">
                                        <span className="visuallyhidden">Mon Calendrier</span>
                                    </a>
                                </li>
                            {/* } */}
                        </ul>
                    </div>
                    {/* SI IL N'EST PAS NOUVEAU */}
                        {/* SI IL est dans un groupe */}
                            <groupInterface />
                        {/* } SINON { */}
                            <groupCreate />
                        {/* } */}
                    {/* } SINON { */}
                        <tutorialInterface />
                    {/* } */}
                </div>
                <adminInterface />
            </div>
        </div>
    );
}