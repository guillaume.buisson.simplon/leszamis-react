
export default function Breadcrumb() {

    const createBreadcrumb = () => {
        const url = window.location.href.split("/");
        url.splice(0, 3)
        const breadcrumb = url.map((elem) => {
            if (elem === url[url.length -1]) {
                return <li key={elem} className="myFont mx-1 myLinkActive user-select-none">{decodeURI(elem) + " >"}</li>
            } else {
                if(elem !== "home") {
                    return <li key={elem} className="myFont mx-1"><a className="myLink" href={"/home/" + elem}>{decodeURI(elem) + " >"}</a></li>
                } else {
                    return <li key={elem} className="myFont mx-1"><a className="myLink" href={"/" + elem}>{decodeURI(elem) + " >"}</a></li>
                }
            }
        })

        return(
            <ol className="breadcrumb p-5">
                {breadcrumb}
            </ol>
        )
    }

    return createBreadcrumb();
}