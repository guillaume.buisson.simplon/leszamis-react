
export default function LoaderManager({visible}) {


    return(
        <div className="d-flex position-fixed top-0 w-100 vh-100 align-items-center justify-content-center">
            <div className={`spinner-border ${visible ? "d-block" : "d-none"}`} role="status"/>
        </div>
    )
}