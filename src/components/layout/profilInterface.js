import React from "react";

export default function ProfilInterface() {
    const Username = 'SoTwinky';
    const Avatar = 'SoTwinky';
    const ID = '1';

    return(
        <nav className="profil">
            <div className="image">
                <div className="img_profil">
                    <img className="image" src={Avatar} onerror="this.onerror=null; this.remove();"/>
                </div>
                <a href="/profil" className="btnProfil"><span className="visuallyhidden">Modifier mon profil</span></a>
            </div>
            <div className="pseudo">
                <div>
                    <span>{Username}</span>
                    <span className="ID">{ID}</span>
                </div>
            </div>
            {/* SI SUR PROFIL { */}
                <a href="index.php" className="btnIndex"><span className="visuallyhidden">Revenir au site</span></a>
            {/* } SINON { */}
                <a href="?profil" className="btnProfil"><span className="visuallyhidden">Modifier mon profil</span></a>
            {/* } */}
            <a href="include/logout.php" className="deconnexion"><span className="visuallyhidden">Déconnexion</span></a>
        </nav>
    )
}